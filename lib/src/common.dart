import 'package:flutter/widgets.dart';

import 'future_widget.dart';
import 'stream_widget.dart';

/// Signature for a function that builds a widget from available data.
typedef DataBuilderFn<T> = Widget Function(BuildContext context, T data);

/// Signature for a function that builds a widget from an exception.
typedef ErrorBuilderFn = Widget Function(
    BuildContext context, Object error, StackTrace? stackTrace);

/// Signature for a function that reports a flutter error, e.g.
/// [FlutterError.reportError].
typedef ErrorReporterFn = void Function(FlutterErrorDetails details);

/// Error that is thrown when the future inside a [FutureWidget] or a stream
/// inside a [StreamWidget] throws an error.
class AsyncWidgetUnexpectedError implements Error {
  @override
  final StackTrace? stackTrace;

  AsyncWidgetUnexpectedError(this.stackTrace);
}

class StreamClosedOnLoadingError implements Error {
  @override
  StackTrace? get stackTrace => null;

  @override
  String toString() {
    return 'Implement whenClosedOnNeitherDataNorError';
  }
}
