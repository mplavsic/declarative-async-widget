import 'package:flutter/widgets.dart';

import 'common.dart';

/// A Widget that builds depending on the state of a [Future].
///
/// This is similar to [FutureBuilder] but accepts separate
/// callbacks for each state. Just like the built in builders, the [future]
/// should not be created at build time because it would restart
/// every time an ancestor is rebuilt.
///
/// If [initialData] is provided, it is used in place of the data before one is
/// available.
///
/// If [disposeOnFutureChange] is false, the current data is retained
/// when the [future] instance changes. Otherwise when
/// [disposeOnFutureChange] is true or omitted, the
/// data is reset (and the widget is rebuilt).
///
/// If the asynchronous operation completes with an error this builds
/// [whenError].
/// If [whenError] is not provided [reportError] is called with the
/// [FlutterErrorDetails].
///
/// Example:
///
/// ```dart
/// class ExampleWidget extends StatefulWidget {
///   const ExampleWidget({Key? key}) : super(key: key);
///
///   @override
///   ExampleWidgetState createState() => ExampleWidgetState();
/// }
///
/// class ExampleWidgetState extends State<ExampleWidget> {
///   final Future<int> _getInt = myFuture(false);
///
///   static Future<int> myFuture(bool shouldThrow) async {
///     await Future.delayed(const Duration(seconds: 4));
///     if (shouldThrow) {
///       throw Exception("You can't pass true as a param");
///     }
///     return 4;
///   }
///
///   @override
///   Widget build(BuildContext context) {
///     return FutureWidget<int>(
///       future: _getInt,
///       whenData: (context, data) => Text('My data is: $data'),
///       whenError: (context, error, stackTrace)
///         => Text('Error was thrown: $error'),
///       whenLoading: (context) => const Text('Loading...'),
///     );
///   }
/// }
/// ```
class FutureWidget<T> extends StatefulWidget {
  /// This is the future the widget listens to.
  final Future<T> future;

  /// The default data builder.
  final DataBuilderFn<T> whenData;

  /// The builder that should be called when an error was thrown by the
  /// [future].
  ///
  /// It is always not null with the constructors:
  /// - [FutureWidget] (the default constructor)
  /// - [FutureWidget.noLoading]
  ///
  /// It is always null with the constructors:
  /// - [FutureWidget.noError]
  /// - [FutureWidget.onlyData]
  ///
  /// In the latter cases no error is expected. Should an error be thrown, an
  /// [AsyncWidgetUnexpectedError] will be thrown at runtime.
  final ErrorBuilderFn? whenError;

  /// The builder that should be called when no data is available and no error
  /// was thrown by the future.
  ///
  /// It is always null with the constructors (in these cases the
  /// [initialData] builder is alway not null):
  /// - [FutureWidget.noLoading]
  /// - [FutureWidget.onlyData]
  ///
  /// It is always not null with the constructors (in these cases the
  /// [initialData] builder is always null):
  /// - [FutureWidget] (the default constructor)
  /// - [FutureWidget.noError]
  final WidgetBuilder? whenLoading;

  /// The initial data used before the future has completed.
  ///
  /// It is always null with the constructors (in these cases the
  /// [whenLoading] builder is alway non-nullable):
  /// - [FutureWidget] (the default constructor)
  /// - [FutureWidget.noError]
  ///
  /// It is always non null with the constructors (in these cases the
  /// [whenLoading] builder is always null):
  /// - [FutureWidget.noLoading]
  /// - [FutureWidget.onlyData]
  final T? initialData;

  /// Whether or not the state of this widget should be disposed (and therefore
  /// the widget rebuilt) when the [future] instance changes.
  ///
  /// If false, the current data should be retained.
  final bool disposeOnFutureChange;

  /// Whether or not to print errors to the console.
  final bool printErrorsToConsole;

  /// If provided, overrides the function that prints errors to the console.
  final ErrorReporterFn reportError;

  /// Whether or not we should send a keep alive
  /// notification with [AutomaticKeepAliveClientMixin].
  final bool keepAlive;

  /// Creates a widget that builds depending on the state of a [Future].
  const FutureWidget({
    super.key,
    required this.future,
    required this.whenData,
    required ErrorBuilderFn this.whenError,
    required WidgetBuilder this.whenLoading,
    this.disposeOnFutureChange = true,
    this.printErrorsToConsole = false,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        initialData = null;

  /// Creates a widget that builds depending on the state of a [Future].
  ///
  /// [printErrorsToConsole] is always set to `true` (in case an unexpected
  /// error happens).
  const FutureWidget.noError({
    super.key,
    required this.future,
    required this.whenData,
    required WidgetBuilder this.whenLoading,
    this.disposeOnFutureChange = true,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        printErrorsToConsole = true, // if an unexpected error happens
        initialData = null,
        whenError = null;

  /// Creates a widget that builds depending on the state of a [Future].
  const FutureWidget.noLoading({
    super.key,
    required this.future,
    required T this.initialData,
    required this.whenData,
    required ErrorBuilderFn this.whenError, // not nullable
    this.disposeOnFutureChange = true,
    this.printErrorsToConsole = false,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        whenLoading = null;

  /// Creates a widget that builds depending on the state of a [Future].
  ///
  /// [printErrorsToConsole] is always set to `true` (in case an unexpected
  /// error happens).
  const FutureWidget.onlyData({
    super.key,
    required this.future,
    required T this.initialData,
    required this.whenData,
    this.disposeOnFutureChange = true,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        printErrorsToConsole = true, // if an unexpected error happens
        whenLoading = null,
        whenError = null;
  @override
  State<StatefulWidget> createState() => _FutureWidgetState<T>();
}

class _FutureWidgetState<T> extends State<FutureWidget<T>>
    with AutomaticKeepAliveClientMixin {
  // N.B.: a `_hasData` variable is necessary (i.e. checking for _data != null
  // is not enough) since some state might be of a nullable type, e.g., `int?`.

  bool _hasData = false;
  T? _data;
  Object? _error;
  StackTrace? _stackTrace;

  void _maybeDispose() {
    if (widget.disposeOnFutureChange) {
      _data = null;
      _error = null;
      _stackTrace = null;
      _hasData = false;
    }
  }

  void _initFuture() {
    _maybeDispose();
    final Future<T> future = widget.future;
    if (widget.initialData != null) {
      _hasData = true;
      _data = widget.initialData;
    }
    future.then((T data) {
      if (future != widget.future || !mounted) return; // Skip if future changed
      setState(() {
        _data = data;
        _hasData = true;
        _error = null;
        _stackTrace = null;
      });
    }, onError: _handleError);
  }

  void _handleError(Object error, StackTrace? stackTrace) {
    if (mounted) {
      setState(() {
        _error = error;
        _stackTrace = stackTrace;
        _hasData = false;
        _data = null;
      });
    }
    if (widget.printErrorsToConsole) {
      widget.reportError(FlutterErrorDetails(
        exception: error,
        stack: stackTrace ?? StackTrace.empty,
        context: ErrorDescription('While updating FutureWidget'),
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    _initFuture();
  }

  @override
  void didUpdateWidget(FutureWidget<T> oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.future != oldWidget.future) _initFuture();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (_error != null) {
      // the future has already completed and we have an error
      if (widget.whenError == null) {
        throw AsyncWidgetUnexpectedError(_stackTrace);
      } else {
        return widget.whenError!(context, _error!, _stackTrace);
      }
    }

    if (_hasData) {
      return widget.whenData(context, _data as T);
    }

    // `whenLoading` must be different from `null` here, since if `initialData`
    // would have been provided, it would have passed the `if (_hasData)` check.
    return widget.whenLoading!(context);
  }

  @override
  void dispose() {
    _maybeDispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => widget.keepAlive;
}
