import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

import 'common.dart';

/// A Widget that builds depending on the state of a [Stream].
///
/// This is similar to [StreamBuilder] but accepts separate
/// callbacks for each state. Just like the built in builders, the [stream]
/// should not be created at build time because it would restart
/// every time the ancestor is rebuilt.
///
/// If [stream] is an rxdart [ValueStream] with an existing value, that value
/// will be available on the first build. Otherwise when no data is available
/// this builds either [whenLoading] if provided, or [whenData] with a null
/// value.
///
/// If [initialData] is provided, it is used in place of the value before one is
/// available.
///
/// If [disposeOnStreamChange] is false, the current value is retained
/// when the [stream] instance changes. Otherwise when
/// [disposeOnStreamChange] is true or omitted, the
/// value is reset (and the widget is rebuilt).
///
/// If the asynchronous operation completes with an error this builds
/// [whenError].
/// If [whenError] is not provided [reportError] is called with the
/// [FlutterErrorDetails].
///
/// When [stream] closes, the appropriate callback will be called:
/// - [whenClosedOnData] (defaults to [whenData])
/// - [whenClosedOnError] (defaults to [whenError])
/// - [whenClosedOnLoading]
///
/// If [paused] is true, the [StreamSubscription] used to listen to [stream] is
/// paused.
///
/// Example:
/// ```dart
/// class ExampleWidget extends StatefulWidget {
///   const ExampleWidget({Key? key}) : super(key: key);
///
///   @override
///   ExampleWidgetState createState() => ExampleWidgetState();
/// }
///
/// int a = 4; // global variable
///
/// class ExampleWidgetState extends State<ExampleWidget> {
///   final Stream<int> _getInt =
///       Stream<int>.periodic(const Duration(seconds: 2), (whatIsThis) {
///     print(whatIsThis);
///     ++a;
///     print(a);
///     if (a == 8 || a == 12) {
///       throw Exception("8 and 12 throw an exception");
///     }
///     return a;
///   });
///
///   @override
///   Widget build(BuildContext context) {
///     return Column(
///       children: [
///         StreamWidget<int>.noLoading(
///           stream: _getInt,
///           whenData: (context, data) => Text('My data is: $data'),
///           whenError: (context, error, stackTrace) =>
///               Text('Error was thrown: $error'),
///           initialData: 9,
///           printErrorsToConsole: true,
///         ),
///       ],
///     );
///   }
/// }
/// ```
class StreamWidget<T> extends StatefulWidget {
  /// The stream the widget listens to.
  final Stream<T> stream;

  /// The default data builder.
  final DataBuilderFn<T> whenData;

  /// The builder that should be called when an error was thrown by the stream.
  ///
  /// It is always not null with the constructors:
  /// - [StreamWidget] (the default constructor)
  /// - [StreamWidget.noLoading]
  ///
  /// It is always null with the constructors:
  /// - [StreamWidget.noError]
  /// - [StreamWidget.onlyData]
  ///
  /// In the latter cases no error is expected. Should an error be thrown, an
  /// [AsyncWidgetUnexpectedError] will be thrown at runtime.
  final ErrorBuilderFn? whenError;

  /// The builder that should be called when no data is available and no error
  /// was thrown by the stream.
  ///
  /// It is always null with the constructors (in these cases the
  /// [initialData] is always not null):
  /// - [StreamWidget.noLoading]
  /// - [StreamWidget.onlyData]
  ///
  /// It is always not null with the constructors (in these cases the
  /// [initialData] builder is always null):
  /// - [StreamWidget] (the default constructor)
  /// - [StreamWidget.noError]
  final WidgetBuilder? whenLoading;

  /// The builder that should be called when the stream is successfully closed
  /// and the last emitted value was some data of type [T].
  final DataBuilderFn<T> whenClosedOnData;

  /// The builder that should be called when the stream is closed and the last
  /// emitted value was an error.
  final ErrorBuilderFn? whenClosedOnError;

  /// The builder that should be called when the stream is closed without having
  /// emitted any value (neither data nor error), which is is an edge case.
  ///
  /// If null and a stream closes without emitting anything, a
  /// [StreamClosedOnLoadingError] will be thrown.
  final WidgetBuilder? whenClosedOnLoading;

  /// The initial data used before the future has completed.
  ///
  /// It is always null with the constructors (in these cases the
  /// [whenLoading] builder is alway non-nullable):
  /// - [StreamWidget] (the default constructor)
  /// - [StreamWidget.noError]
  ///
  /// It is always non null with the constructors (in these cases the
  /// [whenLoading] builder is always null):
  /// - [StreamWidget.noLoading]
  /// - [StreamWidget.onlyData]
  final T? initialData;

  /// If [cancelOnError] is `true`, the subscription is automatically canceled
  /// when the first error event is delivered. The default is `false`.
  ///
  /// If set to `true`, you probably also want to set
  /// `printErrorsToConsole` to `true`.
  ///
  /// If `true`, then you only have to define either [whenClosedOnError] or
  /// [whenError] (since their functionality overlaps in this case).
  final bool cancelOnError;

  /// Whether or not the state of this widget should be disposed (and therefore
  /// the widget rebuilt) when the [stream] instance changes.
  ///
  /// If false, the current data should be retained.
  final bool disposeOnStreamChange;

  /// Whether or not to print errors to the console.
  final bool printErrorsToConsole;

  /// Whether or not the stream subscription should be paused.
  final bool paused;

  /// If provided, overrides the function that prints errors to the console.
  final ErrorReporterFn reportError;

  /// Whether or not we should send a keep alive
  /// notification with [AutomaticKeepAliveClientMixin].
  final bool keepAlive;

  /// Creates a widget that builds depending on the state of a [Stream].
  ///
  /// If [whenClosedOnData] is null, [StreamWidget.whenClosedOnData]
  /// will default to [whenData].
  /// If [whenClosedOnError] is null, [StreamWidget.whenClosedOnError]
  /// will default to [whenError].
  const StreamWidget({
    super.key,
    required this.stream,
    required this.whenData,
    required ErrorBuilderFn this.whenError,
    required WidgetBuilder this.whenLoading,
    DataBuilderFn<T>? whenClosedOnData,
    ErrorBuilderFn? whenClosedOnError,
    this.whenClosedOnLoading,
    this.cancelOnError = false,
    this.disposeOnStreamChange = true,
    this.paused = false,
    this.printErrorsToConsole = false,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        initialData = null,
        whenClosedOnData = whenClosedOnData ?? whenData,
        whenClosedOnError = whenClosedOnError ?? whenError;

  /// Creates a widget that builds depending on the state of a [Stream].
  ///
  /// If [whenClosedOnData] is null, [StreamWidget.whenClosedOnData]
  /// will default to [whenData].
  ///
  /// [printErrorsToConsole] is always set to `true` (in case an unexpected
  /// error happens).
  const StreamWidget.noError({
    super.key,
    required this.stream,
    required this.whenData,
    required WidgetBuilder this.whenLoading,
    DataBuilderFn<T>? whenClosedOnData,
    this.whenClosedOnLoading,
    this.disposeOnStreamChange = true,
    this.paused = false,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        initialData = null,
        whenError = null,
        whenClosedOnData = whenClosedOnData ?? whenData,
        whenClosedOnError = null,
        printErrorsToConsole = true, // if an unexpected error happens
        cancelOnError = true // stop stream if an error occurs
  ;

  /// Creates a widget that builds depending on the state of a [Stream].
  ///
  /// If [whenClosedOnData] is null, [StreamWidget.whenClosedOnData]
  /// will default to [whenData].
  /// If [whenClosedOnError] is null, [StreamWidget.whenClosedOnError]
  /// will default to [whenError].
  const StreamWidget.noLoading({
    super.key,
    required this.stream,
    required T this.initialData,
    required this.whenData,
    required ErrorBuilderFn this.whenError, // not nullable
    DataBuilderFn<T>? whenClosedOnData,
    ErrorBuilderFn? whenClosedOnError,
    this.cancelOnError = false,
    this.disposeOnStreamChange = true,
    this.paused = false,
    this.printErrorsToConsole = false,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        whenLoading = null,
        whenClosedOnLoading = null, // there is always initial data
        whenClosedOnData = whenClosedOnData ?? whenData,
        whenClosedOnError = whenClosedOnError ?? whenError;

  /// Creates a widget that builds depending on the state of a [Stream].
  ///
  /// If [whenClosedOnData] is null, [StreamWidget.whenClosedOnData]
  /// will default to [whenData].
  ///
  /// [printErrorsToConsole] is always set to `true` (in case an unexpected
  /// error happens).
  const StreamWidget.onlyData({
    super.key,
    required this.stream,
    required T this.initialData,
    required this.whenData,
    DataBuilderFn<T>? whenClosedOnData,
    this.disposeOnStreamChange = true,
    this.paused = false,
    this.keepAlive = false,
    ErrorReporterFn? reportError,
  })  : reportError = reportError ?? FlutterError.reportError,
        whenError = null,
        whenLoading = null,
        whenClosedOnData = whenClosedOnData ?? whenData,
        whenClosedOnError = null,
        whenClosedOnLoading = null, // there is always initial data
        printErrorsToConsole = true, // if an unexpected error happens
        cancelOnError = true // stop stream if an error occurs
  ;

  @override
  State<StatefulWidget> createState() => _StreamWidgetState<T>();
}

class _StreamWidgetState<T> extends State<StreamWidget<T>>
    with AutomaticKeepAliveClientMixin {
  // N.B.: a `_hasData` variable is necessary (i.e. checking for _data != null
  // is not enough) since some state might be of a nullable type, e.g., `int?`.

  bool _hasData = false;
  T? _data;
  Object? _error;
  StackTrace? _stackTrace;

  bool _isClosed = false;
  StreamSubscription? _subscription;

  void _maybeDispose() {
    if (widget.disposeOnStreamChange) {
      _data = null;
      _error = null;
      _stackTrace = null;
      _hasData = false;
    }
    _isClosed = false;
    _subscription?.cancel();
    _subscription = null;
  }

  void _togglePaused() {
    if (_subscription != null) {
      if (widget.paused && !_subscription!.isPaused) {
        _subscription!.pause();
      } else if (!widget.paused && _subscription!.isPaused) {
        _subscription!.resume();
      }
    }
  }

  void _initStream() {
    _maybeDispose();
    final Stream<T> stream = widget.stream;
    var skipFirst = false;
    if (stream is ValueStream<T> && stream.hasValue) {
      skipFirst = true;
      _hasData = true;
      _data = stream.value;
      // _error and _stackTrace must be null here
    } else if (widget.initialData != null) {
      _hasData = true;
      _data = widget.initialData;
    }
    _subscription = stream.listen(
      (T event) {
        if (skipFirst) {
          skipFirst = false;
          return;
        }
        setState(() {
          _hasData = true;
          _data = event;
          _error = null;
          _stackTrace = null;
        });
      },
      onDone: _handleDone,
      onError: _handleError,
      cancelOnError: widget.cancelOnError,
    );
  }

  void _handleDone() {
    setState(() {
      _isClosed = true;
    });
  }

  void _handleError(Object error, StackTrace? stackTrace) {
    if (mounted) {
      setState(() {
        _error = error;
        _stackTrace = stackTrace;
        _hasData = false;
        _data = null;
      });
    }
    if (widget.printErrorsToConsole) {
      widget.reportError(FlutterErrorDetails(
        exception: error,
        stack: stackTrace ?? StackTrace.empty,
        context: ErrorDescription('While updating StreamWidget'),
      ));
    }
  }

  @override
  void initState() {
    super.initState();

    _initStream();
    _togglePaused();
  }

  @override
  void didUpdateWidget(StreamWidget<T> oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.stream != oldWidget.stream) _initStream();

    _togglePaused();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (_isClosed) {
      if (_error != null) {
        if (widget.whenClosedOnError == null) {
          throw AsyncWidgetUnexpectedError(_stackTrace);
        } else {
          return widget.whenClosedOnError!(context, _error!, _stackTrace);
        }
      } else if (_hasData) {
        return widget.whenClosedOnData(context, _data as T);
      } else if (widget.whenClosedOnLoading != null) {
        return widget.whenClosedOnLoading!(context);
      } else {
        throw StreamClosedOnLoadingError();
      }
    }

    if (_error != null) {
      // the stream emitted an error
      if (widget.whenError == null) {
        throw AsyncWidgetUnexpectedError(_stackTrace);
      } else {
        return widget.whenError!(context, _error!, _stackTrace);
      }
    }

    if (_hasData) {
      return widget.whenData(context, _data as T);
    }

    // `whenLoading` must be different from `null` here, since if `initialData`
    // would have been provided, it would have passed the `if (_hasData)` check.
    return widget.whenLoading!(context);
  }

  @override
  void dispose() {
    _maybeDispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => widget.keepAlive;
}
