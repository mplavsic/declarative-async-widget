import 'dart:async';

import 'package:declarative_async_widget/declarative_async_widget.dart';
import 'package:flutter/material.dart';

/// A simple wrapper.
///
/// The [streamController.stream] provided by the [builder] should be passed to
/// a [StreamWidget].
///
/// What is given is a local future provider (which is a function) and not
/// simply a future because the future, once awaited, would not be executed
/// again.
class StreamWidgetFutureWrapper<T> extends StatefulWidget {
  final Future<T> Function() futureProvider;
  final Widget Function<T>(
      BuildContext context,
      StreamController<T> streamController,
      bool isRefreshing,
      Future<bool> Function() refresh) builder;

  const StreamWidgetFutureWrapper(
      {Key? key, required this.futureProvider, required this.builder})
      : super(key: key);

  @override
  StreamWidgetFutureWrapperState<T> createState() =>
      StreamWidgetFutureWrapperState<T>();
}

class StreamWidgetFutureWrapperState<T>
    extends State<StreamWidgetFutureWrapper> {
  final StreamController<T> _streamController = StreamController();

  bool _isRefreshing = false;

  /// Once at least data or error.
  bool _hasFiredOnce = false;

  Future<void> computeFuture() async {
    try {
      final res = (await widget.futureProvider()) as T;
      _streamController.add(res);
    } catch (e) {
      _streamController.addError(e);
    }
  }

  @override
  void initState() {
    super.initState();
    () async {
      await computeFuture();
      setState(() {
        _hasFiredOnce = true;
      });
    }();
  }

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  /// Returns true if
  Future<bool> refresh() async {
    if (_hasFiredOnce) {
      if (!_isRefreshing) {
        setState(() {
          _isRefreshing = true;
        });
        await computeFuture();
        setState(() {
          _isRefreshing = false;
        });
        return true;
      }
      // TODO: use log instead of print
      print('StreamWidgetFutureWrapper: refresh is already requested');
      return false;
    } else {
      // TODO: use log instead of print
      print('StreamWidgetFutureWrapper: has not fired yet');
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder(context, _streamController, _isRefreshing, refresh);
  }
}
