import 'dart:async';

import 'package:declarative_async_widget/declarative_async_widget.dart';
import 'package:flutter/material.dart';

// TODO: avoid this! look at main3

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('App bar'),
        ),
        body: const _Body(),
      ),
    );
  }
}

class _Body extends StatefulWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  __BodyState createState() => __BodyState();
}

class __BodyState extends State<_Body> {
  // late final Stream<List<int>> myStream;

  // Future<List<int>> myFuture() async {
  //   await Future.delayed(const Duration(seconds: 2));
  //   return [1,2,3];
  // }

  final StreamController<List<int>> sc = StreamController();
  bool isRefreshing = false;

  @override
  void initState() {
    super.initState();
    // myStream = Stream.fromFuture(() async {
    //   return [1, 2, 3];
    // });
    () async {
      await Future.delayed(const Duration(seconds: 2));
      sc.add([1, 5, 3]);
    }();
  }

  @override
  void dispose() {
    sc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          StreamWidget(
            stream: sc.stream,
            whenData: (context, data) => Text(
                '${data.toString()}${isRefreshing ? " (refreshing)" : ""}'),
            whenError: (context, error, stacktrace) =>
                Text('error${isRefreshing ? " (refreshing)" : ""}'),
            whenLoading: (context) => const Text('loading'),
            disposeOnStreamChange: false,
          ),
          TextButton(
            child: const Text('rerun'),
            onPressed: () async {
              if (!isRefreshing) {
                setState(() {
                  isRefreshing = true;
                });
                await Future.delayed(const Duration(seconds: 2));
                sc.add([1, 2, 3]);
                setState(() {
                  isRefreshing = false;
                });
              }
            },
          ),
        ],
      ),
    );
  }
}
