import 'dart:async';

import 'package:declarative_async_widget/declarative_async_widget.dart';
import 'package:flutter/material.dart';

// TODO: DELETE OR MARK AS NOT OPTIMAL. TYPE SAFETY IS LOST (no <T> casting).
// TODO: Look at the correct solution in main3.dart

void main() {
  runApp(const MyApp());
}

int a = 0;

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('App bar'),
        ),
        body: StreamWidgetFutureWrapper<dynamic>(
          futureProvider: () => Future.delayed(Duration(seconds: 2), () => a++),
          builder: <int>(context, streamController, isRefreshing, refresh) {
            return Center(
              child: Column(
                children: [
                  StreamWidget(
                    stream: streamController.stream,
                    whenData: (context, data) => Text(
                        '${data.toString()}${isRefreshing ? " (refreshing)" : ""}'),
                    whenError: (context, error, stacktrace) =>
                        Text('error${isRefreshing ? " (refreshing)" : ""}'),
                    whenLoading: (context) => const Text('loading'),
                    disposeOnStreamChange: false,
                  ),
                  TextButton(
                    child: const Text('rerun'),
                    onPressed: () async {
                      await refresh();
                      print('refreshed!');
                    },
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
