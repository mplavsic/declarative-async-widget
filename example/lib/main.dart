import 'package:declarative_async_widget/declarative_async_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(),
        body: const Center(
          child: ExampleWidget(),
        ),
      ),
    );
  }
}

class ExampleWidget extends StatefulWidget {
  const ExampleWidget({Key? key}) : super(key: key);

  @override
  ExampleWidgetState createState() => ExampleWidgetState();
}

int a = 4; // global variable

class ExampleWidgetState extends State<ExampleWidget> {
  final Stream<int> _getInt =
      Stream<int>.periodic(const Duration(seconds: 2), (whatIsThis) {
    print(whatIsThis);
    ++a;
    print(a);
    if (a == 8 || a == 12) {
      throw Exception("8 and 12 throw an exception");
    }
    return a;
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        StreamWidget<int>.noLoading(
          stream: _getInt,
          whenData: (context, data) => Text('My data is: $data'),
          whenError: (context, error, stackTrace) =>
              Text('Error was thrown: $error'),
          // whenLoading: (context) => const Text('Loading...'),
          // disposeOnStreamChange: false,
          initialData: 9,
          printErrorsToConsole: true,
        ),
      ],
    );
  }
}
