## 0.4.0

Initial `StreamWidgetFutureWrapper` added.

## 0.3.3

Precisely explains how `async_builder` and this package differ in the README.

## 0.3.2

Bugfix: make `initialData` required in `StreamWidget.onlyData`.

## 0.3.1

Remove heading in README

## 0.3.0

- Breaking change: move `cancelOnError` to initializer list in `StreamWidget.noError` and `StreamWidget.onlyData` constructors (setting it to `true`).
- Add `StreamWidget` documentation.
- Improve `FutureWidget` documentation.
- Improve also other parts of the documentation.

## 0.2.0

- Breaking changes:
    - Force `printErrorsToConsole = true` in `noError` and `onlyData` constructors of both `FutureWidget` and `StreamWidget` (in the initializer list).
    - Remove some optional parameters from some `StreamWidget` constructors anf force them to `null` or `false` in the initializer list.
- Update README.

## 0.1.0

- Multiple breaking changes. Consult commit history.
- Internal logic simplified.
- Example added.
- Update README.

## 0.0.3

Update README.

## 0.0.2

Update LICENSE.

## 0.0.1

Initial release includes `FutureWidget` and `StreamWidget`.
